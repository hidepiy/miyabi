

本チュートリアルでは、インフラストラクチャ層に属するクラス(RepositoryImpl)をドメイン層のパッケージ(todo.domain)に格納しているが、 完全に層別にパッケージを分けるのであれば、インフラストラクチャ層のクラスは、todo.infra以下に作成した方が良い。

ただし、通常のプロジェクトでは、インフラストラクチャ層が変更されることを前提としていない(そのような前提で進めるプロジェクトは、少ない)。 そこで、作業効率向上のために、ドメイン層のRepositoryインタフェースと同じ階層に、RepositoryImplを作成しても良い。


No need to prepare interface for service if there would be only 1 implementation

bean: property, getter & setter (including below)
    dto: DataTransferObject
    entity: DB 1 record
    form: web input/output
vo: ValueObject (Monday, 105 yen,...)


dozer
BeanUtils (commons)
BeanUtils (spring)
ModelMapper