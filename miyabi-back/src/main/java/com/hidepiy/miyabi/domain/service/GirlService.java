package com.hidepiy.miyabi.domain.service;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hidepiy.miyabi.domain.model.Girl;
import com.hidepiy.miyabi.domain.repository.GirlRepository;

@Service
@Transactional
public class GirlService {
    private static final long MAX_UNFINISHED_COUNT = 5;

    @Inject
    GirlRepository girlRepository;

    @Transactional(readOnly = true)
    public Girl findOne(String girlId) {
        return girlRepository.findOne(girlId);
    }

    @Transactional(readOnly = true)
    public Collection<Girl> findAll() {
        return girlRepository.findAll();
    }

    public Girl create(Girl girl) {
        long unfinishedCount = girlRepository.countByFinished(false);
        if (unfinishedCount >= MAX_UNFINISHED_COUNT) {
            throw new RuntimeException();
        }
        String girlId = UUID.randomUUID().toString();
        girl.setGirlId(girlId);
        girl.setCreatedAt(new Date());
        girl.setFinished(false);
        girlRepository.save(girl);
        return girl;
    }

    public Girl finish(String girlId) {
        Girl girl = findOne(girlId);
        if (girl.isFinished()) {
            throw new RuntimeException();
        }
        girl.setFinished(true);
        girlRepository.save(girl);
        return girl;
    }

    public void delete(String girlId) {
        Girl girl = findOne(girlId);
        girlRepository.delete(girl);

    }
}
