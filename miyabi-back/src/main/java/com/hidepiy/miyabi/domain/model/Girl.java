package com.hidepiy.miyabi.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "girl")
public class Girl implements Serializable {
    @Id
    private String girlId;
    private String girlName;
    private boolean finished;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    public String getGirlId() {
        return girlId;
    }
    public void setGirlId(String girlId) {
        this.girlId = girlId;
    }
    public String getGirlName() {
        return girlName;
    }
    public void setGirlName(String girlName) {
        this.girlName = girlName;
    }
    
    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
    
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
