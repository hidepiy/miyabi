package com.hidepiy.miyabi.domain.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hidepiy.miyabi.domain.model.Girl;

public interface GirlRepository extends JpaRepository<Girl, Serializable> {
    @Query("SELECT COUNT(g) FROM Girl g WHERE g.finished = :finished")
    long countByFinished(@Param("finished") boolean finished);
}
