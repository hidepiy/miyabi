package com.hidepiy.miyabi.api.sample;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
public class SampleController {
    
    private static final String TEMPLATE = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

  @RequestMapping("/api/v1/simple")
  public List<String> resource() {
    return IntStream.generate(() -> new Random().nextInt(42))
      .limit(2 + new Random().nextInt(10))
      .mapToObj(Integer::toString)
      .collect(Collectors.toList());
  }
  
  @RequestMapping(value="/api/v1/hello", method=RequestMethod.GET)
  public @ResponseBody SampleResource sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
      return new SampleResource(counter.incrementAndGet(), String.format(TEMPLATE, name));
  }
}
