package com.hidepiy.miyabi.api.sample;

public class SampleResource {
    private final long id;
    private final String contents;
    
    public SampleResource(long id, String title) {
        this.id = id;
        this.contents = title;
    }
    
    public long getId() {
        return id;
    }
    
    public String getTitle() {
        return contents;
    }
}
