package com.hidepiy.miyabi.api.girl;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.dozer.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hidepiy.miyabi.domain.service.GirlService;

@RestController
@RequestMapping("/api/v1/girls")
public class GirlRestController {
    @Inject
    GirlService girlService;
    @Inject
    Mapper mapper;
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<GirlResource> getGirls() {
        return girlService.findAll().stream()
                .map(girl -> mapper.map(girl, GirlResource.class))
                .collect(Collectors.toList());
    }
}
