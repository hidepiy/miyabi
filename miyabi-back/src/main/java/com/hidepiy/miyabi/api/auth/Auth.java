package com.hidepiy.miyabi.api.auth;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Auth implements Serializable {
  private String userName;
  private String token;
  private boolean authenticated;

  public Auth() {
  }

  public Auth(String userName, String token, boolean authenticated) {
    this.userName = userName;
    this.token = token;
    this.authenticated = authenticated;
  }

  public String getUserName() {
    return userName;
  }

  public String getToken() {
    return token;
  }

  public boolean isAuthenticated() {
    return authenticated;
  }
}
