package com.hidepiy.miyabi.api.girl;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuppressWarnings("serial")
public class GirlResource implements Serializable {
    private String girlId;
    @NotNull
    @Size(min = 1, max = 30)
    private String girlName;
    private boolean finished;
    private Date createdAt;
    
    public String getGirlId() {
        return girlId;
    }
    public void setGirlId(String girlId) {
        this.girlId = girlId;
    }
    public String getGirlName() {
        return girlName;
    }
    public void setGirlName(String girlName) {
        this.girlName = girlName;
    }
    public boolean isFinished() {
        return finished;
    }
    public void setFinished(boolean finished) {
        this.finished = finished;
    }
    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
